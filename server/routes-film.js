const express = require('express');

const {getSuggestion, getChosen, addVote, updateChosen, removeSuggestion, getHistory, addToHistory, removeFromHistory, getSearchAvailability, updateSearchAvailability} = require("./models/films")

let router = express.Router();

router.get('/suggestions', async (req, res) => {
    //id: req.query.id,
    getSuggestion().then( (response) => {
        res.status(200).json(response);
    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

router.get('/chosen', async (req, res) => {
    //id: req.query.id,
    getChosen().then( (response) => {
        res.status(200).json(response);
    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

router.get('/getSearch', async (req, res) => {
    //id: req.query.id,
    getSearchAvailability().then( (response) => {
        res.status(200).json(response);
    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

router.get('/history', async (req, res) => {
    getHistory().then( (response) => {
        res.status(200).json(response);
    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

router.post('/vote', async (req, res) => {
    let film = req.body;
    addVote(film).then( () => {

        getSuggestion().then( (response) => {
            res.status(200).json(response);
        }).catch( () => {
            let error = {
                success: false,
                message: "Erreur"
            };
            res.status(500).json(error);
        });

    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

router.post('/choose', async (req, res) => {
    let film = req.body;
    updateChosen(film).then( () => {
        removeSuggestion(film.id).then( () => {
            getChosen().then( (response) => {
                res.status(200).json(response);
            }).catch( () => {
                let error = {
                    success: false,
                    message: "Erreur"
                };
                res.status(500).json(error);
            });
        }).catch( () => {
            let error = {
                success: false,
                message: "Erreur"
            };
            res.status(500).json(error);
        });
    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

router.post('/changeSearch', async (req, res) => {
    let availability = req.body.value;
    updateSearchAvailability(availability).then( () => {
        let response = {
            success: true,
            message: "Ok"
        };
        res.status(200).json(response);
    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

router.post('/archive', async (req, res) => {
    let film = req.body.film;
    let date = req.body.date;
    addToHistory(film, date).then( (response) => {
       
        res.status(200).json(response);
    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

router.post('/unarchive', async (req, res) => {
    let date = req.body.date;
    removeFromHistory(date).then( (response) => {
        res.status(200).json(response);
    }).catch( () => {
        let error = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(error);
    });
});

module.exports = router;
