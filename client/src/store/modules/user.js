import axios from 'axios';
const http = axios.create({
    baseURL: process.env.VUE_APP_BACKEND_URL,
});

const state = {
    isConnected: false,
    isSearchAvailable: true
};

const getters = {};

const actions = {
    set({ commit }, isConnected) {
        commit("setState", isConnected);
    },

    getSearch({ commit }) {
        http.get("/getSearch")
        .then(response => {
            commit("setAvailability", response.data.value);
        })
        .catch(response => {
            console.log(response.data);
        });
    },

    setSearch({ commit }, availability) {
        http.post("/changeSearch", {value:availability})
        .then(() => {
            commit("setAvailability", availability);
        })
        .catch(response => {
            console.log(response.data);
        });
    },
};

const mutations = {
    setState(state, isConnected) {
        state.isConnected = isConnected;
    },

    setAvailability(state, availability) {
        state.isSearchAvailable = availability;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
