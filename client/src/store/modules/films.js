import axios from 'axios';
const http = axios.create({
    baseURL: process.env.VUE_APP_BACKEND_URL,
});

const state = {
    filmsSuggeres : [],
    filmsChoisis : [],
    historique : [],
};

const getters = {};

const actions = {
    getSuggestions({ commit }) {
        http.get("/suggestions")
        .then(response => {
            commit("setSuggestions", response.data);
        })
        .catch(response => {
            console.log(response.data);
        });
    },

    getChosen({ commit }) {
        http.get("/chosen")
        .then(response => {
            commit("setChosen", response.data);
        })
        .catch(response => {
            console.log(response.data);
        });
    },

    getHistory({ commit }) {
        http.get("/history")
        .then(response => {
            commit("setHistory", response.data);
        })
        .catch(response => {
            console.log(response.data);
        });
    },

    addToHistory({ commit },film) {
        http.post("/archive", film)
        .then(response => {
            commit("setHistory", response.data);
        })
        .catch(response => {
            console.log(response.data);
        });
    },

    removeFromHistory({ commit }, date) {
        http.post("/unarchive", date)
        .then(response => {
            commit("setHistory", response.data);
        })
        .catch(response => {
            console.log(response.data);
        });
    },

    vote({ commit }, film){
        http.post("/vote", film)
        .then(response => {
            commit("setSuggestions", response.data);
        })
        .catch(response => {
            console.log(response.data);
        });
    },

    choose({ commit }, film){
        http.post("/choose", film)
        .then(response => {
            commit("setChosen", response.data);
        })
        .catch(response => {
            console.log(response.data);
        });
    }
};

const mutations = {
    setSuggestions(state, films) {
        state.filmsSuggeres = films;
    },
    setChosen(state, films) {
        state.filmsChoisis = films;
    },
    setHistory(state, films) {
        state.historique = films;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
