import Vue from "vue";
import Vuex from "vuex";
import films from "./modules/films";
import user from "./modules/user";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
    modules: {
        films,
        user
    },
    strict: debug
});
