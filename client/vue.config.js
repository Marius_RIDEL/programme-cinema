module.exports = {
    outputDir: '../server/public/',
}


process.env.VUE_APP_BACKEND_URL = process.env.NODE_ENV === "production" ? "https://pas-de-lezard.herokuapp.com/api/" : "http://localhost:8000/api/";
process.env.VUE_APP_API_URL = "https://api.themoviedb.org/3/";
process.env.VUE_APP_API_SECRET = "15d2ea6d0dc1d476efbca3eba2b9bbfb";