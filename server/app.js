const express = require('express');
const cors = require('cors');
var history = require('connect-history-api-fallback');

const routesFilm = require('./routes-film');

let app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use('/api', routesFilm);
app.use(history());
app.use('/', express.static('./public'));

let port = process.env.PORT || 8000;
let host = process.env.HOST || "0.0.0.0";
app.listen(port, host, err => {
  if (err) {
    console.error(err);
  }
  else console.log(`Serveur en route sur ${host}:${port} `);
});
