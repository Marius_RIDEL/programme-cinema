import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Buefy from 'buefy';
import VueCookie from 'vue-cookie';
import axios from "axios";


library.add(fas,far,fab);
Vue.component('vue-fontawesome', FontAwesomeIcon);

Vue.use(Buefy,  {
    defaultIconComponent: 'vue-fontawesome',
    defaultIconPack: 'fas',
});

Vue.use(VueCookie);

Vue.config.productionTip = false;

const http = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
});
http.interceptors.request.use((config) => {
    config.params = config.params || {};
    config.params['api_key'] = process.env.VUE_APP_API_SECRET;
    return config;
});

Vue.prototype.$http = http;


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
