import Vue from 'vue'
import Router from 'vue-router'
import VueCookie from 'vue-cookie'

import mainPage from '@/components/Main'
import loginPage from '@/components/Login'
import adminPage from '@/components/Admin'
import suggestionPage from '@/components/Suggestion'
import rulesPage from '@/components/Rules'
import historyPage from '@/components/History'
import error404 from '@/components/404'
//import eventPage from '@/components/Event'

Vue.use(Router);
Vue.use(VueCookie);
function isConnected(){
    return Vue.cookie.get('userConnected');
}
export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'main',
            component: mainPage,
        },
        {
            path:'/admin',
            name:'admin',
            component: adminPage,
            beforeEnter(to, from, next) {
                if (isConnected()) {
                    next();
                } else {
                    next('/log');
                }
            }

        },
        {
            path:'/admin/:day([1-7])',
            name:'adminEdit',
            component: adminPage,
            props: true,
        },
        {
            path:'/suggest',
            name:'suggestion',
            component:suggestionPage
        },
        {
            path: '/rules',
            name: 'rules',
            component: rulesPage,
        },
        {
            path: '/history',
            name: 'history',
            component: historyPage,
        },
        {
            path: '/log',
            name: 'login',
            component: loginPage,
            beforeEnter(to, from, next) {
                if (isConnected()) {
                    next('/');
                } else {
                    next();
                }
            }
        },
        {
            path: '/404',
            name: '404',
            component: error404
        },
        {
            path: '*',
            redirect: '/404'
        },
        /*{
            path: '/event',
            name: 'event',
            component: eventPage,
        },*/
    ]
});