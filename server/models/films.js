const mongoose = require('mongoose');

const url = "mongodb+srv://Titi:mdpsupersecret@polytech-irfyo.gcp.mongodb.net/Cine?retryWrites=true&w=majority";

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

const SuggestionSchema = new mongoose.Schema({
    id : {type: String, required: true},
    title : {type: String, required: true},
    vote : {type: Number, required: true}
});

const ChosenSchema = new mongoose.Schema({
    id : {type: String, required: true},
    day : {type: Number, required: true},
    title : {type: String, required: true}
});

const HistorySchema = new mongoose.Schema({
    id : {type: String, required: true},
    date : {type: Date, required: true, unique: true},
    title : {type: String, required: true}
});

const SettingsSchema = new mongoose.Schema({
    name : {type: String, required: true},
    value : {type: Boolean, required: true},
});

const Suggestions = mongoose.model('Suggestions', SuggestionSchema, 'votes');
const Chosen = mongoose.model('Choisis', ChosenSchema, 'choisi');
const History = mongoose.model('Historique', HistorySchema, 'historique');
const Settings = mongoose.model('Paramètres', SettingsSchema, 'parametres');

module.exports = {
    getSuggestion: function(){
        return Suggestions.find().sort({'vote': -1});
    },

    addVote: function(film){
        return Suggestions.updateOne(
            { id: film.id },
            { 
                $set: { title: film.title },
                $inc: { vote: 1 }
            },
            { upsert: true }
        );
    },

    removeSuggestion: function(id){
        return Suggestions.deleteOne({ id: id });
    },

    getChosen: function(){
        return Chosen.find().sort({'day': 1});
    },

    updateChosen: function(film){
        return Chosen.updateOne(
            { day: film.day },
            { 
                $set: {
                    id: film.id,
                    title: film.title 
                },
            }
        );
    },

    getHistory: function(){
        return History.find().sort({'date': -1});
    },
    
    addToHistory: function(film, date){
        return History.updateOne(
            { date: date },
            { 
                $set: {
                    id: film.id,
                    title: film.title 
                },
            },
            { upsert: true }
        );
    },
    
    removeFromHistory: function(date){
        return History.deleteOne({ date: date });
    },

    getSearchAvailability: function(){
        return Settings.findOne({ name: 'searchAvailability' }, 'value');
    },
    
    updateSearchAvailability: function(value){
        return Settings.updateOne(
            { name: 'searchAvailability' },
            { 
                $set: { value: value }
            },
            { upsert: true }
        );
    }
};
